#!/usr/bin/env python2.7
## epiladder.py for  in /home/eax/dev
## 
## Made by kevin soules
## Login   <soules_k@epitech.net>
## 
## Started on  Sat May 18 17:10:57 2013 kevin soules
## Last update Tue Sep 23 14:18:56 2014 eax
## 
## ----------------------------------------------------------------------------
## "THE BEER-WARE LICENSE" (Revision 42):
## <kevin@libdev.so> wrote this file. As long as you retain this notice you
## can do whatever you want with this stuff. If we meet some day, and you think
## this stuff is worth it, you can buy me a beer in return.
## ----------------------------------------------------------------------------
## 

import sys
import urllib2
import urllib
import json
from cookielib import CookieJar
import os.path
import operator
import argparse
import getpass
import time

opener = None

def printAvailableCity():
    url = "https://intra.epitech.eu/user/filter/location?format=json"
    response = urllib.urlopen(url)
    out = response.read()
    data = json.loads("\n".join(out.split("\n")[1::]))
    for v in data:
        print v['title'],

def cityToCode(city):
    url = "https://intra.epitech.eu/user/filter/location?format=json"
    response = urllib.urlopen(url)
    out = response.read()
    data = json.loads("\n".join(out.split("\n")[1::]))
    for v in data:
        if v['title'].lower() == city.lower():
            return v['code']
    sys.stderr.write("There is no epitech in this city: %s\n" % city)
    exit()

def login(user, pwd):
    global opener
    cj = CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    data = urllib.urlencode({"login" : user, "password" : pwd})
    try:
        r = opener.open('https://intra.epitech.eu/', data)
    except urllib2.HTTPError as e:
        if e.code == 403:
            sys.stderr.write("403 Error. Wrong login/password ?\n")
            exit()
        else:
            raise

def getPromo(url):
    response = opener.open(url)
    out = response.read()
    data = json.loads("\n".join(out.split("\n")[1::]))
    promo = data["items"]
    while len(promo) < data["total"]:
        response = opener.open(url + "&offset=%d" % len(promo))
        out = response.read()
        data = json.loads("\n".join(out.split("\n")[1::]))
        promo += data["items"]
    return promo

def printProgress(cur_i, max_i, size, s):
    progress = cur_i / float(max_i) * 100
    if cur_i == max_i:
        s1 = '#' * int(size)
    else:
        s1 = '#' * int(progress / size - 1)
        s1 += ['|', '/', '-', '\\'][int(cur_i) % 4] + ' ' * (size - 1 - len(s1))
    sys.stderr.write("\r[ %s ] %2.f%% %s" % (s1, progress, s))
    sys.stderr.flush()
    if progress == 100:
        sys.stderr.write("\n")

def getInfosOnUsers(users):
    infos = []
    i = 0
    l = len(users)
    for user in users:
        url = "https://intra.epitech.eu/intra/user/%s/?format=json" % user
        response = opener.open(url)
        out = response.read()
        infos.append(json.loads("\n".join(out.split("\n")[1::])))
        printProgress(i, l - 1, 10, "(%d logins: %8s)" % (l, user))
        time.sleep(0.2)
        i += 1
    return infos

def sortInfos(infos):
    dico = {}
    for i in infos:
        try:
            dico[i['login']] = [i['gpa'][0]['gpa'], i['groups'][0]['name']]
        except IndexError:
            pass
    sorted_dico = sorted(dico.iteritems(), key=operator.itemgetter(1))
    return sorted_dico

def parseArgs():
    parser = argparse.ArgumentParser(
        description="A simple epiladder-like script.",
        epilog="WARNING: This script will send as many request as ther is people in the sum of city you choose. Please don't abuse of this script. It's why a txt file is generated for each city/promo, to try to prevent overload on the server.",
        usage="%(prog)s [-h] [-l USER] [-p PWD] (-t {1,2,3,4,5} -c CITY [CITY ...] | --list) [--version]")
    parser.add_argument("-l", "--login", "-u", "--user", nargs='+', dest="user", default=None, help="Your login, and if more than one, other people you want to highlight.")
    parser.add_argument("-p", "--password", type=str, dest="pwd", default=None, help="Your password")
    parser.add_argument("-t", "--tek", type=int, dest="tek", choices=xrange(1, 6), help="tek1/tek2...")
    parser.add_argument("-c", "--city", dest="city", nargs='+', help="Witch city to get stats of")
    parser.add_argument("-o", "--output", dest="output", type=argparse.FileType('w'), default=sys.stdout, help="File where to save the output.")
    parser.add_argument("-i", "--intra", action="store_true", help="Get users only from intra. Not from temp files.")
    parser.add_argument("--list", "--list-city", action="store_true", help="List available city and exit.")
    parser.add_argument('--version', action='version', version='%(prog)s 0.2')
    args = parser.parse_args()
    if (not args.city or not args.tek) and not args.list:
        parser.error('Either (-c/--city AND -t/--tek) OR --list/--list-city is required.') 
    return args

def genFileName(code, tek):
    return "%s-tek%d.txt" % (code.replace("/", "_"), tek)

def fillCodes(args):
    codes = {}
    for c in args.city:
        codes[c] = cityToCode(c)
        code = codes[c]
        tmpFile = genFileName(code, args.tek)
        if args.intra and os.path.isfile(tmpFile):
            os.unlink(tmpFile)
        if (args.intra or not os.path.isfile(tmpFile)) and (args.user == None or args.pwd == None):
            if args.user == None:
                args.user = [raw_input("User:")]
            if args.pwd == None:
                args.pwd = getpass.getpass("Password:")
            login(args.user[0], args.pwd)
    return codes

def getCityResult(tek, code):
    tmpFile = genFileName(code, tek)
    if os.path.isfile(tmpFile):
        infos = json.load(open(tmpFile))
        sys.stderr.write("infos from tmp file\n")
    else:
        promo = getPromo('https://intra.epitech.eu/user/filter/user?format=json&location=%s&year=2014&course=bachelor/classic&active=true&promo=tek%d' % (code, tek))
        infos = getInfosOnUsers([u["login"] for u in promo])
        tmpf = open(tmpFile, "w")
        tmpf.write(json.dumps(infos))
        tmpf.close()
        sys.stderr.write("infos from intra\n")
    return infos

def printResult(result, args):
    i = len(result)
    for v in result:
        color_prefix = ""
        color_suffix = ""
        user = v[0]
        gpa = v[1][0]
        city = v[1][1]
        if args.user and v[0] in args.user and args.output.isatty():
            color_prefix = "\033[32m"
            color_suffix = "\033[0m"
        args.output.write("%s%d\t -> %s\t%s (%s)%s\n" % (color_prefix, i, user, gpa, city, color_suffix))
        i -= 1

if __name__ == "__main__":
    args = parseArgs()
    if args.list:
        printAvailableCity()
        exit()

    result = []
    codes = fillCodes(args)
    for c in args.city:
        code = codes[c]
        result += getCityResult(args.tek, code)
        time.sleep(1)

    srt = sortInfos(result)
    printResult(srt, args)
